//
//  ViewController.swift
//  DemoCapillary
//
//  Created by Thai Ba Tuan on 7/9/19.
//  Copyright © 2019 Thai Ba Tuan. All rights reserved.
//

import UIKit
import SwiftGRPC
import SwiftProtobuf
import FirebaseInstanceID
import Tink
import Security.CipherSuite
import UserNotifications

class ViewController: UIViewController, UNUserNotificationCenterDelegate {
    
    var rsa : RsaUtils?
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var ConnectButton: UIButton!
    @IBOutlet weak var PortTextField: UITextField!
    @IBOutlet weak var hostTextField: UITextField!
    @IBOutlet weak var SentTextField: UITextField!
    @IBOutlet weak var OupuText: UITextView!
    @IBOutlet weak var genKey: UIButton!
    @IBOutlet weak var regUser: UIButton!
    @IBOutlet weak var regKey: UIButton!
    @IBOutlet weak var connect: UIButton!
    let keyId = "com.example.tidy.vn"
    var channel = Channel(address: "192.168.43.200:8443", certificates: Constans().cerlocalStirng)
    
    func genUserToken() {
        print("get device Token")
        InstanceID.instanceID().instanceID { (result, error) in
            let token = result?.token
            print("device token: \(String(describing: token))")
            UserDefaults.standard.set(token, forKey: "USER_TOKEN_KEY")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        regUser.addTarget(self, action: #selector(RegisterUser), for: .touchUpInside)
        regKey.addTarget(self, action: #selector(RegKey), for: .touchUpInside)
        connect.addTarget(self, action: #selector(sendMessageRequest), for: .touchUpInside)
        genKey.addTarget(self, action: #selector(genNewKey), for: .touchUpInside)
        ConnectButton.addTarget(self, action: #selector(connectHandel), for: .touchUpInside)
        disconnectButton.addTarget(self, action: #selector(dissConnectHandel), for: .touchUpInside)
        rsa = RsaUtils.init()
        genUserToken()
    
    }
    
    @objc func connectHandel() {
        hostTextField.isEnabled = false
        PortTextField.isEnabled = false
        let address = "\(hostTextField.text!):\(PortTextField.text!)"
        print("address: \(address)")
        channel = Channel(address: address, certificates: Constans().cerlocalStirng)
    }
    
    @objc func dissConnectHandel() {
        hostTextField.isEnabled = true
        hostTextField.isEnabled = true
        channel = Channel(address: "")
    }
    
    func decryptFromAndroid() {
        let privateKey = try? self.rsa!.getPrivateKey(keychainId: self.keyId, isAuth: false)
        
        // decrypt
        guard SecKeyIsAlgorithmSupported(privateKey!, .decrypt, .rsaEncryptionPKCS1) else {
            print("not supported")
            return
        }
        
        let data = Data(base64Encoded: "haha")
        var error: Unmanaged<CFError>?

        guard let decrytCfData = SecKeyCreateDecryptedData(privateKey!, .rsaEncryptionPKCS1, data! as CFData, &error) else {
            print("can't decrypt")
            return
        }
        let decryptData = decrytCfData as Data
        let notification = try? SecureNotification(serializedData: decryptData)
        print("decryptText : \(String(describing: notification?.title))")
    }
    
    @objc func genNewKey() {
        //dell Key
        self.rsa?.deleteKeyPair(String: self.keyId, boolean: false)
        try? self.rsa?.generateKeyPair(keychainId: self.keyId, isAuth: false)
    }
    
    @objc func sendMessageRequest(){
        var noti = SecureNotification()
        noti.id = Int32(Date().timeIntervalSince1970)
        noti.body = "Title: Sent From iOS device"
        
        if SentTextField.text == nil {
            noti.title = "No Body"
        } else {
            noti.title = SentTextField.text ?? "No Body"
        }
        
        var message = SendMessageRequest()
        message.keyAlgorithm = .rsaEcdsa
        message.userID = getKeychainUniqueId()
        message.isAuthKey = false
        message.delaySeconds = 2
        message.deviceOs = "iOS"
        let data = try! noti.serializedData()
        message.data = data
        let testService = try? CapillaryServiceServiceClient(channel: channel).sendMessage(message)
        let text = "notification to data: \(data.base64EncodedString())\n" + "TestServide: \(String(describing: testService))"
        OupuText.text = text
    }
    
    @objc func RegisterUser(){
        var message = AddOrUpdateUserRequest()
        message.userID = getKeychainUniqueId()
        message.token = Utils().userToken
        let testService = try? CapillaryServiceServiceClient(channel: channel).addOrUpdateUser(message)
        OupuText.text = "TestServide: \(String(describing: testService))"
        
    }
    
    @objc func RegKey(){
        var message = AddOrUpdatePublicKeyRequest()
        message.algorithm = KeyAlgorithm.rsaEcdsa
        message.isAuth = false
        message.userID = getKeychainUniqueId()
        let publicKey = try? self.rsa?.getPublicKeyString(keychainId: self.keyId, isAuth: false)
        var pulbicKeyCapillary = CapillaryPublicKey()
        pulbicKeyCapillary.isAuth = false
        pulbicKeyCapillary.keychainUniqueID = getKeychainUniqueId()
        pulbicKeyCapillary.serialNumber = Int32(220)
        var wrapPublicKey = WrappedRsaEcdsaPublicKey()
        wrapPublicKey.keyBytes = Data(base64Encoded: publicKey!!)!
        wrapPublicKey.padding = "OAEP"
        pulbicKeyCapillary.keyBytes = try! wrapPublicKey.serializedData()
        message.keyBytes = try! pulbicKeyCapillary.serializedData()
        let testService = try? CapillaryServiceServiceClient(channel: channel).addOrUpdatePublicKey(message)
        
        let text = "TestServide: \(String(describing: testService))"
        OupuText.text = text
        
    }
    
    func getKeychainUniqueId() -> String{
        var uniqueId = UserDefaults.standard.string(forKey: Constans().KEYCHAIN_UNIQUE_ID_KEY)
        if (uniqueId == nil) {
            uniqueId = UUID.init().uuidString
            UserDefaults.standard.set(uniqueId, forKey: Constans().KEYCHAIN_UNIQUE_ID_KEY)
            }
        return uniqueId!;
    }
    
    private static func toSerialNumberPrefKey(isAuth: Bool) -> String {
        return isAuth ? Constans().AUTH_KEY_SERIAL_NUMBER_KEY : Constans().NO_AUTH_KEY_SERIAL_NUMBER_KEY;
    }
    
    var senderVerifier : TINKPublicKeyVerify!
    let SIGNATURE_LENGTH_BYTES_LENGTH = 4;
    
    func showTextInOutputView(text: String) {
        OupuText.text = "Can't Decrypt Text, Text sent from Server: \n" + text
    }
    
    func decodeText(data: Data) {
        // Senderverify
//        guard (data.count > 13) else {
//            let text = String(data: data, encoding: .utf8)
//            OupuText.text = "Can't Decrypt Text, Text sent from Server: \n" + (text ?? "nothing")
//            return
//        }
        guard let senderUrl = Bundle.main.url(forResource: "sender_verification_key", withExtension: "dat") else {
            print("not hav sender url")
            return
        }
        let dataSenderVerifycation = try? Data(contentsOf: senderUrl)
        let keyReader = try? TINKBinaryKeysetReader.init(serializedKeyset: dataSenderVerifycation!)
        
        //Decypt Text
        let cyterText = data
        // Step One: Verify Text.
        let bytes = [UInt8](cyterText)
        let array = bytes[0...3]
        var value : Int = 0
        for byte in array {
            value = value << 8
            value = value | Int(byte)
        }
        print("value: \(value)")
        let payloadLength = bytes.count - 4 - value
        let singatureData = Data(bytes: Array(bytes[4..<(4+value)]), count: value)
        let decrypBytes = bytes[(4+value)..<(4+value+payloadLength)]
        let newArry = Array(decrypBytes)
        let decryData = Data(bytes: newArry, count: newArry.count)
        try? TINKPublicKeyVerifyFactory.primitive(with: TINKKeysetHandle(cleartextKeysetHandleWith: keyReader!)).verifySignature(singatureData, for: decryData)
        // Step Two Decrypt by using Hybrid Decrytion with privateKey
        // get symmetri cipher text
        let hybridRsaCypherText = try? HybridRsaCiphertext(serializedData: decryData)
        let payloadCiphertext = hybridRsaCypherText?.payloadCiphertext
        let symmetricKeyCiphertext = decryData
        print("payload: \(String(describing: payloadCiphertext)) symmetric: \(String(describing: symmetricKeyCiphertext))")
        
        
        OupuText.text = "Encrypted Text form server: \n" + symmetricKeyCiphertext.base64EncodedString()
        
        var error: Unmanaged<CFError>?
        let privateKey = try? self.rsa!.getPrivateKey(keychainId: keyId, isAuth: false)
        
        // decrypt
        guard SecKeyIsAlgorithmSupported(privateKey!, .decrypt, .rsaEncryptionPKCS1) else {
            print("not supported")
            return
        }
    
        print("symmetric: \(String(describing: symmetricKeyCiphertext.base64EncodedString()))")
        
        guard let decrytText = SecKeyCreateDecryptedData(privateKey!, .rsaEncryptionPKCS1, symmetricKeyCiphertext as CFData, &error) else {
            print("can't decrypt")
            OupuText.text.append("\n Can't decrypt") 
            return
        }
        if error == nil {
            let data = decrytText as Data
            let notification = try? SecureNotification(serializedData: data)
            print("decryptText : \(String(describing: notification?.title))")
            if let text = notification?.title {
                OupuText.text.append("\nText decocde from server:  " + text)
            } else {
                OupuText.text.append("Don't have text from server")
            }
            
        } else {
            print("have error: \(String(describing: error))")
        }
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


