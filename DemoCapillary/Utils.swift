//
//  Utils.swift
//  DemoCapillary
//
//  Created by Thai Ba Tuan on 7/12/19.
//  Copyright © 2019 Thai Ba Tuan. All rights reserved.
//

import Foundation

class Utils {
    func getUserId() -> String{
        var userId = Constans.init().userDefaults.string(forKey: "USER_ID_KEY")
        if userId == nil {
            userId = UUID.init().uuidString
            Constans.init().userDefaults.set(userId, forKey: "USER_ID_KEY")
        }
        return userId!
    }
    
    lazy var userToken : String = {
        var token = Constans().userDefaults.string(forKey: "USER_TOKEN_KEY")
        return token!
    }()
    
    let userDefault = UserDefaults.standard
}
