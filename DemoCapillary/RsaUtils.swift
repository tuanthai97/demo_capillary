//
//  AndroidKeyStoreRsaUtils.swift
//  genkey
//
//  Created by Ba Tuan on 7/6/19.
//  Copyright © 2019 Ba Tuan. All rights reserved.
//

import Foundation

final class RsaUtils {
    private let AUTH_KEY_ALIAS_SUFFIX = "_capillary_rsa_auth";
    private let NO_AUTH_KEY_ALIAS_SUFFIX = "_capillary_rsa_no_auth";
    private let KEYSTORE_ANDROID = "AndroidKeyStore";
    private let KEY_SIZE = 2048;
    private let KEY_DURATION_YEARS = 100;
    // Allow any screen unlock event to be valid for up to 1 hour.
    private static let UNLOCK_DURATION_SECONDS = 60 * 60;
    
    func generateKeyPair(keychainId : String, isAuth : Bool) throws {
        print("debug::on generateKeyPair")
        
        let tag = keychainId.data(using: .utf8)!
        let attributes: [String: Any] =
            [kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
             kSecAttrKeySizeInBits as String: 2048,
             kSecPrivateKeyAttrs as String:
                [kSecAttrIsPermanent as String: true,
                 kSecAttrApplicationTag as String: tag]
        ]
        
        var error: Unmanaged<CFError>?
        if #available(iOS 10.0, *) {
            guard let privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &error) else {
                print("Genkey not success")
                throw (error?.takeRetainedValue())!
            }
            
            let publicKey = SecKeyCopyPublicKey(privateKey)
            
            var error:Unmanaged<CFError>?
            
            if let cfdata = SecKeyCopyExternalRepresentation(publicKey!, &error) {
                let data:Data = cfdata as Data
                let b64Key = data.base64EncodedString()
                print("b64Key \(b64Key)")
            }
            
            let tagPrivateKey = "com.tuan.test.crypto.\(keychainId).privateKey".data(using: .utf8)!
            let addPrivateKeyQuery: [String: Any] = [kSecClass as String: kSecClassKey,
                                                     kSecAttrApplicationTag as String: tagPrivateKey,
                                                     kSecValueRef as String: privateKey]
            
            let tagPublicKey = "com.tuan.test.crypto.\(keychainId).publicKey".data(using: .utf8)!
            let addPublicKeyQuery: [String: Any] = [kSecClass as String: kSecClassKey,
                                                    kSecAttrApplicationTag as String: tagPublicKey,
                                                    kSecValueRef as String: publicKey!]
            
            let statusStorePrivateKey = SecItemAdd(addPrivateKeyQuery as CFDictionary, nil)
            guard statusStorePrivateKey == errSecSuccess else {
                print("debug::nostore")
                return
            }
            
            let statusStorePublicKey = SecItemAdd(addPublicKeyQuery as CFDictionary, nil)
            guard statusStorePublicKey == errSecSuccess else {
                print("debug::nostore")
                return
            }
            
            print("Gen Key Success")
        }
    }
     func getPublicKeyString(keychainId : String , isAuth : Bool) throws -> String  {
        
        let tagPublicKey = "com.tuan.test.crypto.\(keychainId).publicKey".data(using: .utf8)!
        let getquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tagPublicKey,
                                       kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
                                       kSecReturnData as String: true,
                                       kSecAttrKeyClass as String: kSecAttrKeyClassPublic]
        
        var item: AnyObject?
        var key = ""
        let status = SecItemCopyMatching(getquery as CFDictionary, &item)
        if status == errSecSuccess {
            print("Have key")
            if let data = item as? Data {
                print("cast complete")
                let exportImport = CryptoExportImportManager()
                if let exportDerKey = exportImport.exportPublicKeyToDER(data, keyType: kSecAttrKeyTypeRSA as String, keySize: 2048){
                    let exportPem = exportImport.PEMKeyFromDERKey(exportDerKey)
                    print("publickey need: \(exportPem)")
                    key = exportPem
                }
            } else {
                print("can't cast")
            }
        } else {
            print("Not have key")
        }
        return key
    }
    
    func getPublicKey(keychainId : String) throws -> SecKey  {
        
        let tagPublicKey = "com.tuan.test.crypto.\(keychainId).publicKey".data(using: .utf8)!
        let getquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tagPublicKey,
                                       kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
                                       kSecReturnRef as String: true]
        
        var item: CFTypeRef?
        let status = SecItemCopyMatching(getquery as CFDictionary, &item)
        if status == errSecSuccess {
            print("Have key")
        } else {
            print("Not have key")
        }
        
        let key = item as! SecKey
        return key
    }
    
    func getPrivateKey(keychainId : String , isAuth : Bool) throws -> SecKey  {
        
        let tagPrivateKey = "com.tuan.test.crypto.\(keychainId).privateKey".data(using: .utf8)!
        let getquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tagPrivateKey,
                                       kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
                                       kSecReturnRef as String: true]
        
        var item: CFTypeRef?
        let status = SecItemCopyMatching(getquery as CFDictionary, &item)
        if status == errSecSuccess {
            print("Have key")
        } else {
            print("Not have key")
        }
        
        let key = item as! SecKey
        
        return key
    }
    
    func getPrivateKeyData(keychainId : String , isAuth : Bool) throws -> Data  {
        
        let tagPrivateKey = "com.tuan.test.crypto.\(keychainId).privateKey".data(using: .utf8)!
        let getquery: [String: Any] = [kSecClass as String: kSecClassKey,
                                       kSecAttrApplicationTag as String: tagPrivateKey,
                                       kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
                                       kSecReturnData as String: true]
        
        var item: AnyObject?
        let status = SecItemCopyMatching(getquery as CFDictionary, &item)
        if status == errSecSuccess {
            print("Have key")
        } else {
            print("Not have key")
        }
        
        let key = item as! Data
        
        print("private text: \(key.base64EncodedString())")
        return key
    }

    
    func deleteKeyPair(String keychainId : String, boolean isAuth : Bool) {
        //var alias = toKeyAlias(keychainId: keychainId, isAuth: isAuth)
        let tagPrivateKey = "com.tuan.test.crypto.privateKey.\(keychainId)".data(using: .utf8)!
        let deleteQuery: [String: Any] = [kSecClass as String: kSecClassKey,
                                          kSecAttrApplicationTag as String: tagPrivateKey,
                                          kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
                                          kSecReturnRef as String: true]
        let status = SecItemDelete(deleteQuery as CFDictionary)
        if status == errSecSuccess {
            print("Delete Success")
        } else {
            print("Don't")
        }
    }
    
    func toKeyAlias(keychainId : String, isAuth : Bool) -> String {
        let suffix  = isAuth ? AUTH_KEY_ALIAS_SUFFIX : NO_AUTH_KEY_ALIAS_SUFFIX;
        return keychainId + suffix;
    }
    
    func checkKeyExists(keyStore : String, keychainId : String, isAuth: Bool) -> Void{
        //checkKeyExists(keyStore, toKeyAlias(keychainId, isAuth));
    }
    
    private func checkKeyExists(keyStore : String, alias : String) {
//        if (!keyStore.containsAlias(alias)) {
//            print("android key store has no rsa key pair with alias " + alias);
//        }
    }
    
//    static Padding getCompatibleRsaPadding() {
//        return VERSION.SDK_INT >= VERSION_CODES.M ? Padding.OAEP : Padding.PKCS1;
//    }
    
}
