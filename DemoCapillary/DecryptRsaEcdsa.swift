//
//  File.swift
//  DemoCapillary
//
//  Created by Thai Ba Tuan on 7/23/19.
//  Copyright © 2019 Thai Ba Tuan. All rights reserved.
//

import Foundation
import Tink
import SwiftProtobuf
class Decryp{
    var senderVerifier : TINKPublicKeyVerify!
    let SIGNATURE_LENGTH_BYTES_LENGTH = 4;
    func decodeText() {
        // Senderverify
        let sender_verification = Bundle.main.url(forResource: "sender_verification_key", withExtension: "dat")
        let data_sender = try? Data(contentsOf: sender_verification!)
        let keyReader = try? TINKBinaryKeysetReader(serializedKeyset: data_sender!)
        let key = try? TINKKeysetHandle(cleartextKeysetHandleWith: keyReader!)
        senderVerifier = try? TINKPublicKeyVerifyFactory.primitive(with: key!)
        
        //Decypt Text
        
        let textDecode = "AAAATAEoc7fjMEUCIQCVqpRqVxgr+/5QcVPS6tZVNy+RnRbjgtrdsiYWcA+SkwIgV09mVyzGL4NZTR5XVLcRyWzuNyGCQ9mQNlR91cQtPc4KgAI0UuQmqOlw+dVoT2X5uSXjqZZipqxT0CdkFmJTUavY1aeE0m0BMoq55k96XzZ7yEBb9g7xGaqlqzLslkEhKWtclPJIl3XsPV9qqk3VDggQizbBoCF3xCBoNexF+DkcyXGeVozEaLBQDrYQxxe51UZyIhyVdqv875CbJyZPiN0IROQlayChYEFy1we0asge3pMdQnfeE7TEQKOCNlUasjM/AUMkaukMMQJSMEmq7vhYJN2xaOcPamzMoxCVq6MEpcSR3nK0Y7iLEHheGP51pvlPBaKhkBkygJffNDWbBCJcAiFfO4OPy5Ch7wfbiKYwh9aZypes1dkTO69oHfQ7JfZ3ElcBV9TUy5DWcf21rk/HYOvUlBxG/hiOocnm2l2tbpaYazOvdHHGwfl0mhZbyTBCMDB9okSIZyhgEzI8B9PXBNRRXorv6O1wxg7DLUig2oeNag+ULtGXyco="
        let cyterText = Data(base64Encoded: textDecode)
        // Step One: Verify Text.
        let bytes = [UInt8](cyterText!)
        
        let array = bytes[0...3]
        var value : Int = 0
        for byte in array {
            value = value << 8
            value = value | Int(byte)
        }
        let payloadLength = bytes.count - 4 - value
        let singatureData = Data(bytes: Array(bytes[4..<(4+value)]), count: value)
        let decrypBytes = bytes[(4+value)..<(4+value+payloadLength)]
        let newArry = Array(decrypBytes)
        let decryData = Data(bytes: newArry, count: newArry.count)
        try! senderVerifier.verifySignature(singatureData, for: decryData)
    }
}

public class ByteBuffer {
    
    public init(size: Int) {
        array.reserveCapacity(size)
    }
    
    public func allocate(_ size: Int) {
        array = [UInt8]()
        array.reserveCapacity(size)
        currentIndex = 0
    }
    
    public func nativeByteOrder() -> Endianness {
        return hostEndianness
    }
    
    public func currentByteOrder() -> Endianness {
        return currentEndianness
    }
    
    public func order(_ endianness: Endianness) -> ByteBuffer {
        currentEndianness = endianness
        return self
    }
    
    public func put(_ value: UInt8) -> ByteBuffer {
        array.append(value)
        return self
    }
    
    public func put(_ value: Int32) -> ByteBuffer {
        if currentEndianness == .little {
            array.append(contentsOf: to(value.littleEndian))
            return self
        }
        
        array.append(contentsOf: to(value.bigEndian))
        return self
    }
    
    public func put(_ value: Int64) -> ByteBuffer {
        if currentEndianness == .little {
            array.append(contentsOf: to(value.littleEndian))
            return self
        }
        
        array.append(contentsOf: to(value.bigEndian))
        return self
    }
    
    public func put(_ value: Int) -> ByteBuffer {
        if currentEndianness == .little {
            array.append(contentsOf: to(value.littleEndian))
            return self
        }
        
        array.append(contentsOf: to(value.bigEndian))
        return self
    }
    
    public func put(_ value: Float) -> ByteBuffer {
        if currentEndianness == .little {
            array.append(contentsOf: to(value.bitPattern.littleEndian))
            return self
        }
        
        array.append(contentsOf: to(value.bitPattern.bigEndian))
        return self
    }
    
    public func put(_ value: Double) -> ByteBuffer {
        if currentEndianness == .little {
            array.append(contentsOf: to(value.bitPattern.littleEndian))
            return self
        }
        
        array.append(contentsOf: to(value.bitPattern.bigEndian))
        return self
    }
    
    public func get() -> UInt8 {
        let result = array[currentIndex]
        currentIndex += 1
        return result
    }
    
    public func get(_ index: Int) -> UInt8 {
        return array[index]
    }
    
    public func getInt32() -> Int32 {
        let result = from(Array(array[currentIndex..<currentIndex + MemoryLayout<Int32>.size]), Int32.self)
        currentIndex += MemoryLayout<Int32>.size
        return currentEndianness == .little ? result.littleEndian : result.bigEndian
    }
    
    public func getInt32(_ index: Int) -> Int32 {
        let result = from(Array(array[index..<index + MemoryLayout<Int32>.size]), Int32.self)
        return currentEndianness == .little ? result.littleEndian : result.bigEndian
    }
    
    public func getInt64() -> Int64 {
        let result = from(Array(array[currentIndex..<currentIndex + MemoryLayout<Int64>.size]), Int64.self)
        currentIndex += MemoryLayout<Int64>.size
        return currentEndianness == .little ? result.littleEndian : result.bigEndian
    }
    
    public func getInt64(_ index: Int) -> Int64 {
        let result = from(Array(array[index..<index + MemoryLayout<Int64>.size]), Int64.self)
        return currentEndianness == .little ? result.littleEndian : result.bigEndian
    }
    
    public func getInt() -> Int {
        let result = from(Array(array[currentIndex..<currentIndex + MemoryLayout<Int>.size]), Int.self)
        currentIndex += MemoryLayout<Int>.size
        return currentEndianness == .little ? result.littleEndian : result.bigEndian
    }
    
    public func getInt(_ index: Int) -> Int {
        let result = from(Array(array[index..<index + MemoryLayout<Int>.size]), Int.self)
        return currentEndianness == .little ? result.littleEndian : result.bigEndian
    }
    
    public func getFloat() -> Float {
        let result = from(Array(array[currentIndex..<currentIndex + MemoryLayout<UInt32>.size]), UInt32.self)
        currentIndex += MemoryLayout<UInt32>.size
        return currentEndianness == .little ? Float(bitPattern: result.littleEndian) : Float(bitPattern: result.bigEndian)
    }
    
    public func getFloat(_ index: Int) -> Float {
        let result = from(Array(array[index..<index + MemoryLayout<UInt32>.size]), UInt32.self)
        return currentEndianness == .little ? Float(bitPattern: result.littleEndian) : Float(bitPattern: result.bigEndian)
    }
    
    public func getDouble() -> Double {
        let result = from(Array(array[currentIndex..<currentIndex + MemoryLayout<UInt64>.size]), UInt64.self)
        currentIndex += MemoryLayout<UInt64>.size
        return currentEndianness == .little ? Double(bitPattern: result.littleEndian) : Double(bitPattern: result.bigEndian)
    }
    
    public func getDouble(_ index: Int) -> Double {
        let result = from(Array(array[index..<index + MemoryLayout<UInt64>.size]), UInt64.self)
        return currentEndianness == .little ? Double(bitPattern: result.littleEndian) : Double(bitPattern: result.bigEndian)
    }
    
    
    public enum Endianness {
        case little
        case big
    }
    
    private func to<T>(_ value: T) -> [UInt8] {
        var value = value
        return withUnsafeBytes(of: &value, Array.init)
    }
    
    private func from<T>(_ value: [UInt8], _: T.Type) -> T {
        return value.withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: T.self)
        }
    }
    
    private var array = [UInt8]()
    private var currentIndex: Int = 0
    
    private var currentEndianness: Endianness = .big
    private let hostEndianness: Endianness = OSHostByteOrder() == OSLittleEndian ? .little : .big
}
