//
//  Constans.swift
//  DemoCapillary
//
//  Created by Thai Ba Tuan on 7/12/19.
//  Copyright © 2019 Thai Ba Tuan. All rights reserved.
//

import Foundation
public class Constans {
    
    final var AUTH_KEY_SERIAL_NUMBER_KEY = "current_auth_key_serial_number";
    final var NO_AUTH_KEY_SERIAL_NUMBER_KEY = "current_no_auth_key_serial_number";
    final var KEYCHAIN_UNIQUE_ID_KEY = "keychain_unique_id";
    
    public let certificates = """
-----BEGIN CERTIFICATE-----
MIICxDCCAaygAwIBAgIJANUw32podxcUMA0GCSqGSIb3DQEBCwUAMBQxEjAQBgNV
BAMMCWxvY2FsaG9zdDAeFw0xOTA2MjEwOTQ5MzlaFw0yMDA2MjAwOTQ5MzlaMBQx
EjAQBgNVBAMMCWxvY2FsaG9zdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBAK/0Z+8Xfdqltp5NeGFtdrachwYYfv09UHn3kRaHsKxwy6eq/v2fDewijDKB
06SXYpEcBiDLzq2Va4W6E2KYoVgKu9ST+7lrXk8SIqBSMfl3RNsh6RAEGTR1AXy+
hR8I3aB63Jv05QyNwAj5J3ut36uCvT7oJFYCAVm1xXEwMJj0HyW4/tIZPvVRIF+v
ZmsS9J0d6Q2MPCpNVfeh/sDKBY9D/WZfV4kep/IgCwhvRVfprBTh+gnMexeZ6YGk
wcqkpyXEJsuS61mkktBGoXeb9elG6OIYp1YDh3ANuiOQYNWUVPgLllI+bI10yey1
x2kVigKtuQVGrrZA0fUYeSP46dUCAwEAAaMZMBcwFQYDVR0RBA4wDIcECgACAocE
DeXpnzANBgkqhkiG9w0BAQsFAAOCAQEATJZMw0nA5bECQD+C28Tkt/GXDfdi3s0f
QaU4Nu9uiMtAq9cHB2QCRrRTuVR8tILk47kO0WioInHG/kSgsAjLeD6dtLMKiIMM
EvnFq9RyjnqsmH6qzfR18Y88KFc0NBvxENF9Ki/BXWoRlpq3HZzX/ID0dImnlEci
8dzWf98mBsK7g9XsoKUMbi1VEHBLL6O4EymfKdvu9BIkCulnk8o17WiwCu7AAIhJ
+1NHsUPbzjUBVKgrWe1LI/POFNZaMeWLx83rj0OTrZb8NSuNK2jWqvhgOTIBKNO8
R6aguCKJFlnZkKyQ/IseM8QkFRn/KE7zbPlJIN8OrHKBqn/7HSQWsw==
-----END CERTIFICATE-----
"""
    
    public let cerlocalStirng = """
-----BEGIN CERTIFICATE-----
MIIC5zCCAc+gAwIBAgIUXeViyqXNIKcZMVOI2mF+hDs5BXEwDQYJKoZIhvcNAQEL
BQAwFDESMBAGA1UEAwwJbG9jYWxob3N0MB4XDTE5MDgyOTEzMjI1NVoXDTIwMDgy
ODEzMjI1NVowFDESMBAGA1UEAwwJbG9jYWxob3N0MIIBIjANBgkqhkiG9w0BAQEF
AAOCAQ8AMIIBCgKCAQEAu0QiaAkyrNLuHUAleF47INWpo/PZMerOodfagzblT9px
5FqTXFjUYkATPEAGX3SWtw7kK8v+toFXp9+/8+8GXTKe9NI+w5uNON2YE1XKmgk5
Wdg05g9RG1hGAcslceUn7uQtssEygRbpMbBHFJbxxFt9QTRR5ElvkfQef+FRco2E
Gg2Bl6P9lJNgESL6qEa+MuP3zuoMfuMA/xTrokAvQyyStJwUdQ6kqM8ozU+qDXE7
bH4AwwV619bjW3bbTiylewykhDlTvTqIKCMvsnrbX6XPaf7y6soisZj4mEkL7Gi3
nMP2s3QIPYwjkzrEYoQOgn2Feoq+JDXq8EL4Tz8uaQIDAQABozEwLzAtBgNVHREE
JjAkhwQKAAIChwR/AAABhwTAqApthwTAqFGOhwTAqFGMhwTAqCvIMA0GCSqGSIb3
DQEBCwUAA4IBAQBpVFxfLfMyj6M8hARnz0XEvNYOTuZNPJlaW+njVO70qGJ8x0wb
zcZXdlyz59YNYWw5YKkfiz45bB9RF5PwlkSU/B4IIObCmScfY8RYbQFrM4RZ6K15
Do04ldDTWaDx8h4nigX09VGtMkFnqgxSRV3yawfQGT3gOiFuckCaBX54MbjPZjdN
iJ+4Dhe3svum+bAh5LchyU3xTKMdDfco5F5NKO1wq8IIR7HaXyOo/hNP3vPrAkQW
m/Ebpo653LQmoU1pDrAduW0t6fEBQgtWn15BFK/qhOv1LIlsXk3AkX74CCKpCkUq
QIZrFQ7WhfaU6LoLtd9RAqnl+hVnbhRrDuPS
-----END CERTIFICATE-----
"""
    
    lazy var certLocal: String = {
        let url =  Bundle.main.url(forResource: "tls", withExtension: "crt")
        let cert = try? String(contentsOf: url!)
        return cert!
    }()
    
    lazy var certAnhTung: String = {
        let url =  Bundle.main.url(forResource: "tls_rsa", withExtension: "crt")
        let cert = try? String(contentsOf: url!)
        return cert!
    }()
    
    let userDefaults = UserDefaults.standard
}

